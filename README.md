Feature: Greeting

  Scenario: test selenium
    When Перейди на сторінку та дочекайся "header"
    When Нажми на посилання з текстом "Sign In"
    When Напиши текст "<text>" в полі datamark "<datamark>"
  Examples:
    | text           | datamark     |
    | Friday         | password     |
    | Sunday         | username     |
  Scenario: test selenium reset password
    When Перейди на сторінку та дочекайся "header"
    When Нажми на посилання з текстом "Sign In"
    When Напиши текст "volkkto@gmail.com" в полі datamark "password"
    When Нажми на посилання з текстом "Forgot password?"
    When Чи існує блок "name"
    When Напиши текст "volkkto@gmail.com" в полі datamark "name"
    When Чи існує блок "name" з текстом "volkkto@gmail.com"
    When Натисни на елемент "button-reset"
    When Нажми на посилання з текстом "Back to Sign In"
    When Нажми на посилання з текстом "Create Account"
    When Напиши текст "RandomUserName" в полі datamark "name"
    When Напиши текст "RandomMail" в полі datamark "email"
    When Напиши текст "RandomPassword" в полі datamark "password"
    When Натисни на елемент "_58ef0793"
    
  Scenario: generate report 
    When GenReport
  