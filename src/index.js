const { Builder, By, Key, until } = require('selenium-webdriver');
const webdriver = require('selenium-webdriver');
const firefox = require('selenium-webdriver/firefox');
const { randomMail, randomPassword, randomUserName } = require('./helpers')

let driver = new Builder().forBrowser('firefox').setFirefoxOptions( new firefox.Options().headless(),
    new firefox.Options().addArguments('--proxy-server=http://smwg.ukrenergo.ent:9090'),
    new firefox.Options().addArguments('--disable-gpu'),
    new firefox.Options().addArguments('--no-sandbox'),
    new firefox.Options().addArguments('--headless'),
).build();

global.driver = driver

class Greeter {
    async sayHello(url, string) {
        await driver.get(url);
        await driver.wait(until.elementLocated(By.id(string)), 10000);
    }
    sleep(ms) {
        return new Promise(resolve => setTimeout(resolve, ms));
    }
    async selectOption(selector) {
        driver.findElement(webdriver.By.css(selector))
            .click();
    }
    async clicktext(text) {
        await driver.wait(until.elementLocated(By.linkText(text)), 10000);
        const elements = await driver.findElement(By.linkText(text));
        await elements.click();
    }
    async sendText(text, selector, mode = "standart") {
        await driver.wait(until.elementLocated(By.css(`[name="${selector}"]`)), 10000);
        if (mode == "standart") {
            let elements = await driver.findElement(By.css(`[name="${selector}"]`));
            await elements.sendKeys(text)
        } else if (mode == "pass") {
            let elements = await driver.findElement(By.css(`[name="${selector}"]`));
            await elements.sendKeys(randomPassword())
        } else if (mode == "username") {
            let elements = await driver.findElement(By.css(`[name="${selector}"]`));
            await elements.sendKeys(randomUserName())
        } else if (mode == "mail") {
            let elements = await driver.findElement(By.css(`[name="${selector}"]`));
            await elements.sendKeys(randomMail())
        }


    }
    async checkNewBlock(selector, text) {
        await driver.wait(until.elementLocated(By.xpath(selector)), 10000);
        const table = await driver.findElement(By.xpath(selector));
        const rows = await table.findElements(By.css('tr'));

        for (const row of rows) {
            const cells = await row.findElements(By.css('td'));
            for (const cell of cells) {
                const cellText = await cell.getText();
                console.log(cellText);
                if (cellText === text) {
                    return;
                }
            }
        }

        throw new Error(`checkNewBlock did not find text ${text}`);
    }
    async checkNotBlock(selector, text) {
        await driver.wait(until.elementLocated(By.xpath(selector)), 10000);
        const table = await driver.findElement(By.xpath(selector));
        const rows = await table.findElements(By.css('tr'));

        for (const row of rows) {
            const cells = await row.findElements(By.css('td'));
            for (const cell of cells) {
                const cellText = await cell.getText();
                console.log(cellText);
                if (cellText === text) {
                    throw new Error(`Block in not delete ${text}`);
                }
            }
        }

        return;
    }
    async checkBlock(selector, text) {
        await driver.wait(until.elementLocated(By.id(selector)), 10000);
        const element = await driver.findElement(By.id(selector));

        if (text != null && text != undefined) {
            if (await element.getTagName() == 'input') {
                if (await element.getAttribute("value") != text) {
                    let e = Error(`checkBlock not find text ${text} ---->> ${await element.getAttribute("value")}`)
                    throw e
                }
            } else {
                if (await element.getText() != text) {
                    console.log("не збіг " + await element.getText());
                    Error(`checkBlock not find text ${text} ---->> ${await element.getText()}`)
                }
            }
        }
    }
    async elementClick(selector) {
        //const element = await driver.findElement(`[name="${selector}"]`);
        await driver.wait(until.elementLocated(By.css(selector)), 10000);
        const element = await driver.findElement(By.css(selector));
        await element.click();
    }
    async elementClickXPath(selector) {
        await driver.wait(until.elementLocated(By.xpath(selector)), 10000);
        const element = await driver.findElement(By.xpath(selector));
        await element.click();
    }
    async fileUpload(selector, filePath) {
        const image = path.resolve(filePath)
        await driver.findElement(By.id(selector)).sendKeys(image);
    }
}

module.exports = {
    Greeter
}