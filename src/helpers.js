function randomString (length = 6){
    let result = '';
    const characters = 'abcdefghijklmnopqrstuvwxyz0123456789';
    const charactersLength = characters.length;
    let counter = 0;
    while (counter < length) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
    counter += 1;
    }
    return result;
}

function randomUserName(){
    return "username_" + randomString()
}
function randomPassword(){
    return randomString(12)
}
function randomMail(){
    return `testmail${randomString()}@ua.energy`
}
module.exports.randomMail = randomMail
module.exports.randomUserName = randomUserName
module.exports.randomPassword = randomPassword