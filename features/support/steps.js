const assert = require('assert')
const { When, Then, After, Status } = require('@cucumber/cucumber')
const { Greeter } = require('../../src')
const reporter = require('cucumber-html-reporter');


var test = new Greeter()

After(async function(testCase) {
    if (testCase.result.status === Status.FAILED) {
        const screenshot = await driver.takeScreenshot()
        this.attach(screenshot, { mediaType: 'base64:image/png' })
    }
});



When('Перейди на сторінку {string} та дочекайся {string}', async function(url, expectedResponse) {
    this.whatIHeard = await test.sayHello(url, expectedResponse)
});

When('Нажми на посилання з текстом {string}', async function(expectedResponse) {
    this.whatIHeard = await test.clicktext(expectedResponse);
});

When('Напиши текст {string} в полі datamark {string}', async function(text, selector) {
    if (text == "RandomUserName") {
        this.whatIHeard = await test.sendText(null, selector, "username");
    } else if (text == "RandomPassword") {
        this.whatIHeard = await test.sendText(null, selector, "pass");
    } else if (text == "RandomMail") {
        this.whatIHeard = await test.sendText(null, selector, "mail");
    } else {
        this.whatIHeard = await test.sendText(text, selector);
    }

});

When('Обери пункт за xpath {string}', async function(selector) {
    this.whatIHeard = await test.selectOption(selector);
})

When('Чи існує блок {string}', async function(selector) {
    this.whatIHeard = await test.checkBlock(selector);
});

When('Чи існує блок {string} з текстом {string}', async function(selector, text) {
    this.whatIHeard = await test.checkBlock(selector, text);
});
When('Чи існує новий блок {string} з текстом {string}', async function(selector, text) {
    this.whatIHeard = await test.checkNewBlock(selector, text);
});

When('Чи не існує блок {string} з текстом {string}', async function(selector, text) {
    this.whatIHeard = await test.checkNotBlock(selector, text);
});

When('Зачекай {string} мілісекунд', async function(ms) {
    this.whatIHeard = await test.sleep(ms)
})
When('Натисни на елемент {string}', async function(selector) {
    this.whatIHeard = await test.elementClick(selector)
});
When('Натисни на елемент xpath {string}', async function(selector) {
    this.whatIHeard = await test.elementClickXPath(selector)
});
When('Завантаж файл {string} в {string}', async function(filePath, selector) {
    this.whatIHeard = await test.fileUpload(filePath, selector)
});



When('GenReport', async function() {
    setTimeout(() => {
        var options = {
            theme: 'bootstrap',
            jsonFile: '1.json',
            output: './report/cucumber_report.html',
            reportSuiteAsScenarios: true,
            scenarioTimestamp: true,
            launchReport: true,
            screenshotsDirectory: 'screenshots/',
            storeScreenshots: true,
            metadata: {
                "App Version": "0.3.2",
                "Test Environment": "STAGING",
                "Browser": "Chrome  54.0.2840.98",
                "Platform": "Windows 10",
                "Parallel": "Scenarios",
                "Executed": "Remote"
            },
            failedSummaryReport: true,
        };

        reporter.generate(options);
    }, 3000)
});