Feature: Greeting

  Scenario: test_login
    When Перейди на сторінку "https://test-itsmue.ua.energy/#!/" та дочекайся "loginForm"
    When Напиши текст "root" в полі datamark "username"
    When Напиши текст "132569" в полі datamark "password"
    When Натисни на елемент ".md-raised"
    When Чи існує блок "img-ukrenergo-logo"


  Scenario: Infrastructure add
    When Перейди на сторінку "https://test-itsmue.ua.energy/#!/Infrastructure" та дочекайся "head-menu"
    When Натисни на елемент xpath "/html/body/ng-view/div/md-card/md-card-content/ng-form/div/md-input-container[2]/select"
    When Обери пункт за xpath "#infraservice_select > option:nth-child(2)"
    When Напиши текст "new_infraservices_name" в полі datamark "ism_infraservices_name"
    When Натисни на елемент xpath "/html/body/ng-view/div/md-card/md-card-actions/button[1]"
    When Зачекай "1000" мілісекунд
    When Чи існує новий блок "/html/body/ng-view/div/main/div/table" з текстом "new_infraservices_name"
  
  Scenario: Infrastructure edit
    When Перейди на сторінку "https://test-itsmue.ua.energy/#!/Infrastructure" та дочекайся "head-menu"
    When Натисни на елемент xpath "/html/body/ng-view/div/main/div/table/tbody/tr[last()]/td[2]"
    When Напиши текст "_mod" в полі datamark "ism_infraservices_name"
    When Натисни на елемент xpath "/html/body/ng-view/div/md-card/md-card-actions/button[2]"
    When Зачекай "1000" мілісекунд
    When Чи існує новий блок "/html/body/ng-view/div/main/div/table" з текстом "new_infraservices_name_mod"

  Scenario: Infrastructure edit
    When Перейди на сторінку "https://test-itsmue.ua.energy/#!/Infrastructure" та дочекайся "head-menu"
    When Натисни на елемент xpath "/html/body/ng-view/div/main/div/table/tbody/tr[last()]/td[2]"
    When Натисни на елемент xpath "/html/body/ng-view/div/md-card/md-card-actions/button[3]"
    When Зачекай "1000" мілісекунд
    When Чи не існує блок "/html/body/ng-view/div/main/div/table" з текстом "new_infraservices_name_mod"
  
  Scenario: Status add
    When Перейди на сторінку "https://test-itsmue.ua.energy/#!/Status" та дочекайся "head-menu"
    When Натисни на елемент xpath "/html/body/ng-view/div/md-card/md-card-content/ng-form/div/md-input-container[3]/select"
    When Обери пункт за xpath "#infraservice_select > option:nth-child(2)"
    When Напиши текст "Done" в полі datamark "infraServicesStatus"
    When Натисни на елемент xpath "/html/body/ng-view/div/md-card/md-card-actions/button[1]"
    When Зачекай "1000" мілісекунд
    When Чи існує новий блок "/html/body/ng-view/div/main/div/table" з текстом "new_infraservices_name"



  Scenario: generate report 
    When GenReport
  